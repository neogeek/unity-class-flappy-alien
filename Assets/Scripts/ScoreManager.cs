﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    private GameObject playerObj;
    private PlayerController playerController;
    public Text hudText;

    public int highScore {
        get {
            return PlayerPrefs.GetInt("highScore", 0);
        }
        set {
            PlayerPrefs.SetInt("highScore", value);
        }
    }

    public int distance {
        get {
            return Mathf.RoundToInt(latestPostion.x - startPosition.x);;
        }
    }

    private int _highScore = 0;

    private Vector3 startPosition;
    private Vector3 latestPostion;

    void Awake() {

        playerObj = GameObject.FindWithTag("Player");
        playerController = playerObj.GetComponent<PlayerController>();

    }

    void Start() {

        startPosition = playerObj.transform.position;

    }

    void Update() {

        latestPostion = playerObj.transform.position;

        if (playerController.isAlive) {

            hudText.text = distance.ToString();

        }

    }

}
