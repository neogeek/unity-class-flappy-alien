﻿using UnityEngine;

public class SectionSpawner : MonoBehaviour {

    public GameObject[] sections;

    private Vector3 nextMountPoint = new Vector3(-10, 0, 0);

    void Start() {

        SpawnSection();

    }

    public void SpawnSection() {

        GameObject section = Instantiate(sections[Random.Range(0, sections.Length - 1)], nextMountPoint, Quaternion.identity);

        nextMountPoint = section.transform.Find("Mount Point").gameObject.transform.position;

    }

}
