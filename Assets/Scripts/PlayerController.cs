﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

    public GameObject explosion;
    public ScreenShake screenShake;
    public LevelLoader levelLoader;

    public bool isAlive = true;

    private Rigidbody2D rb;
    private Vector3 originalCameraPosition;

    private readonly float thrust = 20;
    private readonly float speed = 50;

    void Awake() {

        rb = gameObject.GetComponent<Rigidbody2D>();

        originalCameraPosition = Camera.main.transform.position;

    }

    void Start() {

        rb.AddForce(gameObject.transform.right * speed);

    }

    void Update() {

        if (isAlive) {

            Camera.main.transform.position = new Vector3(
                gameObject.transform.position.x + originalCameraPosition.x,
                originalCameraPosition.y,
                originalCameraPosition.z
            );

            if (Input.GetMouseButton(0) || Input.GetButton("Jump")) {

                rb.AddForce(gameObject.transform.up * thrust);

            }

        }

    }

    void OnCollisionEnter2D(Collision2D other) {

        Instantiate(explosion, gameObject.transform.position, Quaternion.identity);

        rb.velocity = Vector3.zero;

        rb.AddForce(other.transform.right * -1000);

        screenShake.Shake();

        isAlive = false;

        levelLoader.RestartLevel();

    }

}
