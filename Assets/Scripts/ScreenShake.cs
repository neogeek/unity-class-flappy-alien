﻿using UnityEngine;

public class ScreenShake : MonoBehaviour {

    private Vector3 originalPosition;

    private float currentIntensity = 0;
    private float currentDuraton = 0;

    void Update() {

        if (currentDuraton > 0) {

            gameObject.transform.position = originalPosition + Random.insideUnitSphere * currentIntensity;

            currentDuraton = Mathf.Max(currentDuraton - Time.deltaTime, 0);

        }

    }

    public void Shake(float duration = 1.0f, float intensity = 0.2f) {

        originalPosition = gameObject.transform.position;

        currentIntensity = intensity;
        currentDuraton = duration;

    }

}
