﻿using UnityEngine;

public class SpawnTrigger : MonoBehaviour {

    private SectionSpawner sectionSpawner;

    void Awake() {

        sectionSpawner = GameObject.Find("SectionSpawner").GetComponent<SectionSpawner>();

    }

    void OnTriggerEnter2D(Collider2D other) {

        if (other.gameObject.tag.Equals("Player")) {

            sectionSpawner.SpawnSection();

        }

    }

}
