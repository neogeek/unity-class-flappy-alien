﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {

    public PlayerController playerController;
    public ScoreManager scoreManager;

    public GameObject highScoreScreen;
    public Text mainMessageText;
    public Text subMessageText;

    public void RestartLevel() {

        StartCoroutine("WaitRestart");

    }

    IEnumerator WaitRestart() {

        highScoreScreen.SetActive(true);

        if (scoreManager.distance > scoreManager.highScore) {

            mainMessageText.text = "New High Score!";

            scoreManager.highScore = scoreManager.distance;

        } else {

            mainMessageText.text = "High score: " + scoreManager.highScore.ToString();

        }

        subMessageText.text = "Your score: " + scoreManager.distance.ToString();

        yield return new WaitForSeconds(2);

        highScoreScreen.SetActive(false);

        SceneManager.LoadScene("Level");

    }

}
